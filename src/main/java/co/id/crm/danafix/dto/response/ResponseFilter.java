package co.id.crm.danafix.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseFilter {
    private String key;
    private Object value;
    private String typeData;
    private String sign;
}
