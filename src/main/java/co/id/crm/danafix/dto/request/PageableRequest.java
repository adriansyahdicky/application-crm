package co.id.crm.danafix.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageableRequest implements Serializable {

    private Integer skip;
    private Integer take;
    private String sort;
    private String filter;

}
