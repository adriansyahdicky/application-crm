package co.id.crm.danafix.util;

import co.id.crm.danafix.dto.response.ResponseFilter;


public class CheckDataType {

    public static SearchOperation operationSearch(ResponseFilter responseFilter){
        SearchOperation operation = null;
        if(responseFilter.getTypeData().equalsIgnoreCase("String")){
            operation =  SearchOperation.MATCH;
        }
        else if(responseFilter.getTypeData().equalsIgnoreCase("Number")){
            operation = SearchOperation.EQUAL;
        }
        else if(responseFilter.getTypeData().equalsIgnoreCase("Date")
                && responseFilter.getSign().equalsIgnoreCase(">=")){
            operation = SearchOperation.GREATER_THAN_EQUAL;
        }
        else if(responseFilter.getTypeData().equalsIgnoreCase("Date")
                && responseFilter.getSign().equalsIgnoreCase("<")){
            operation = SearchOperation.LESS_THAN_EQUAL;
        }

        return operation;
    }

}
