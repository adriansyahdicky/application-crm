package co.id.crm.danafix.service;

import co.id.crm.danafix.dto.request.PageableRequest;
import co.id.crm.danafix.entity.Member;
import org.springframework.data.domain.Page;

public interface MemberService {
    Member getMemberById(Integer id);
    Page<Member> getMemberPage(PageableRequest pageableRequest);
}
