package co.id.crm.danafix.service.impl;

import co.id.crm.danafix.dto.request.PageableRequest;
import co.id.crm.danafix.dto.response.ResponseFilter;
import co.id.crm.danafix.dto.response.ResponseSorting;
import co.id.crm.danafix.entity.Member;
import co.id.crm.danafix.repository.MemberRepository;
import co.id.crm.danafix.service.MemberService;
import co.id.crm.danafix.util.CheckDataType;
import co.id.crm.danafix.util.GenericSpesification;
import co.id.crm.danafix.util.SearchCriteria;
import co.id.crm.danafix.util.SearchOperation;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberRepository memberRepository;

    @Override
    public Member getMemberById(Integer id) {
        return memberRepository.findById(id).get();
    }

    @Override
    public Page<Member> getMemberPage(PageableRequest pageableRequest) {
        Sort sort = Sort.by("id").descending();
        Gson gson = new Gson();

        if(pageableRequest.getSort() != null){
            Type listType = new TypeToken<List<ResponseSorting>>(){}.getType();
            List<ResponseSorting> sorting = gson.fromJson(pageableRequest.getSort(), listType);
            for(int i=0; i<sorting.size(); i++){
                ResponseSorting a = sorting.get(0);
                String selector = a.getSelector();
                boolean desc = a.isDesc();
                sort = desc == true ? Sort.by(selector).descending() : Sort.by(selector).ascending();
            }
        }

        GenericSpesification genericSpesification = new GenericSpesification<Member>();
        Type listType = new TypeToken<List<ResponseFilter>>(){}.getType();
        List<ResponseFilter> filters = gson.fromJson(pageableRequest.getFilter(), listType);

        if(filters != null) {
            for (int i = 0; i < filters.size(); i++) {
                SearchOperation operation = CheckDataType.operationSearch(filters.get(i));
                genericSpesification.add(new SearchCriteria(filters.get(i).getKey(), filters.get(i).getValue(), operation));
            }
        }

        Pageable paging = PageRequest.of(pageableRequest.getSkip(), pageableRequest.getTake(), sort);
        return (Page<Member>) memberRepository.findAll(genericSpesification, paging);
    }
}
