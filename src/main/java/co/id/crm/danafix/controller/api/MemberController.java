package co.id.crm.danafix.controller.api;

import co.id.crm.danafix.dto.request.PageableRequest;
import co.id.crm.danafix.entity.Member;
import co.id.crm.danafix.service.MemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/api/member")
public class MemberController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemberController.class);

    @Autowired
    private MemberService memberService;

    @GetMapping(value = "/get-member-page")
    public ResponseEntity<?> getMemberPage(PageableRequest pageable){
        try{
            Page<Member> getDataMember = memberService.getMemberPage(pageable);
            return ResponseEntity.ok(getDataMember);
        }catch (Exception ex){
            LOGGER.error("error controller getMemberPage : "+ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/get-memberbyid/{id}")
    public ResponseEntity<?> getDetailMember(@PathVariable("id") Long id){
        return ResponseEntity.ok("getdetailmemberbyid");
    }

    @PostMapping(value = "/save-member")
    public ResponseEntity<?> saveMember() {
        return ResponseEntity.ok("SaveMember");
    }

}
