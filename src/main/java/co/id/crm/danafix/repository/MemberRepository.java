package co.id.crm.danafix.repository;

import co.id.crm.danafix.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberRepository extends JpaRepository<Member, Integer>,
        PagingAndSortingRepository<Member, Integer>, JpaSpecificationExecutor<Member> {

    @Query(value = "select * from member where cast(date_of_birth as text)=:date", nativeQuery = true)
    Page<Member> getByDate(@Param("date") String date, Pageable pageable);
}
