package co.id.crm.danafix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrmDanafixApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrmDanafixApplication.class, args);
	}

}
