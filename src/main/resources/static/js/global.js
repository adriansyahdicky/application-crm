function get_uri(){
    return window.location.origin;
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

function dialogConfirm(data){
    var myDialog = DevExpress.ui.dialog.custom({
        title: "Deleted",
        messageHtml: "<b>Are You Sure Deleted this Data !</b>",
        buttons: [{
            text: "Yes",
                onClick: function(e) {
                    return {
                        buttonText: e.component.option("text")
                    }
                }
            },
            {
            text: "No",
                onClick: function(e) {
                   return {
                       buttonText: e.component.option("text")
                    }
                 }
            }]
    });
    myDialog.show().done(function(dialogResult) {
        if(dialogResult.buttonText === "Yes"){
           console.log(dialogResult.buttonText);
           loadLoadingPanel(data);
        }
    });
}

function loadLoadingPanel(dataOptions){
    var loadPanel = $(".loadpanel").dxLoadPanel({
       shadingColor: "rgba(0,0,0,0.4)",
       position: { of: "#gridContainer" },
       visible: false,
       showIndicator: true,
       showPane: true,
       shading: true,
       closeOnOutsideClick: false,
       onShown: function(){
           setTimeout(function () {
               loadPanel.hide();
           }, 3000);
       },
       onHidden: function(){
           console.log("hidden check")
       }
    }).dxLoadPanel("instance");
}
