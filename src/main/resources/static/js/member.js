$(function(){

    function isNotEmpty(value) {
        return value !== undefined && value !== null && value !== "";
    }

    var store = new DevExpress.data.CustomStore({
            key: "id",
            load: function (loadOptions) {
                var deferred = $.Deferred(),
                    args = {};
                [
                    "skip",
                    "take",
                    "sort",
                    "filter"
                ].forEach(function(i) {
                    if (i in loadOptions && isNotEmpty(loadOptions[i])){
                        if(i === "skip"){
                            args[i] = JSON.stringify(loadOptions[i]/10);
                        }
                        else if(i === "filter"){
                            debugger;
                            var columnIndex = "", filterValue = "", key = "", pushJson = [], typeData = "", sign = "";
                            var filtering = loadOptions[i];
                                for(var j=0; j<filtering.length; j++){
                                    key = filtering[0];
                                    sign = filtering[1];
                                    columnIndex = filtering.columnIndex;
                                    filterValue = filtering.filterValue;

                                    if(columnIndex != undefined && filterValue != undefined){
                                        typeData = jQuery.type(filterValue);
                                        filterValue = typeData == "date" ? formatDate(filterValue) : filterValue;
                                        pushJson.push({
                                            key: key,
                                            value: filterValue,
                                            typeData: typeData,
                                            sign: sign
                                        });
                                        args[i] = JSON.stringify(pushJson);
                                        break;
                                    }

                                    if(columnIndex === undefined && filterValue === undefined){
                                        key = filtering[j][0];
                                        sign = filtering[j][1];
                                        columnIndex = filtering[j].columnIndex;
                                        filterValue = filtering[j].filterValue;

                                        if(columnIndex != undefined && filterValue != undefined){
                                             typeData = jQuery.type(filterValue);
                                             filterValue = typeData == "date" ? formatDate(filterValue) : filterValue;
                                             pushJson.push({
                                                key: key,
                                                value: filterValue,
                                                typeData: typeData,
                                                sign: sign
                                             });
                                        }
                                    }
                                }

                                if(filtering.columnIndex === undefined){
                                   args[i] =  JSON.stringify(pushJson);
                                }
                        }
                        else{
                            args[i] = JSON.stringify(loadOptions[i]);
                        }
                    }
                });

                $.ajax({
                    url: get_uri() + "/api/member/get-member-page",
                    dataType: "json",
                    data: args,
                    success: function(result) {
                        deferred.resolve(result.content, {
                            totalCount: result.totalElements
                        });
                    },
                    error: function() {
                        deferred.reject("Data Loading Error");
                    },
                    timeout: 10000
                });
                return deferred.promise();
            }
        });

        $("#gridContainer").dxDataGrid({
                dataSource: store,
                showBorders: true,
                remoteOperations: true,
                paging: {
                    pageSize: 10,
                    pageIndex: 1
                },
//                editing: {
//                     mode: "popup",
//                     allowUpdating: true,
//                     allowDeleting: true,
//                     useIcons: true
//                },
                filterRow: { visible: true },
                columns: [{
                    dataField: "id",
                    caption: "ID Member",
                    dataType: "number",
                    allowFiltering: false
                },
                {
                    dataField: "name",
                    caption: "Name Member",
                    dataType: "string",
                },
                {
                    dataField: "mobile",
                    caption: "Mobile Member",
                    dataType: "string",
                },
                {
                    dataField: "email",
                    caption: "Email Member",
                    dataType: "string",
                },
                {
                   dataField: "age",
                   caption: "Usia",
                   dataType: "number",
                },
                {
                   dataField: "dateOfBirth",
                   caption: "Date Of Birth",
                   dataType: "date",
                   filterOperations: ["<=", ">="],
                   selectedFilterOperation: ">=",
                   format: 'yyyy-MM-dd'
                },
                 {
                     type: "buttons",
                     width: 130,
                     cellTemplate: function (container, options) {
                         $("<div /> ").dxButton({
                           stylingMode: "contained",
                           icon: 'trash',
                           type: "danger",
                           onClick: function (e) {
                               dialogConfirm(options);
                           }
                         }).appendTo(container);

                         $(" <div />").dxButton({
                           stylingMode: "contained",
                           icon: 'edit',
                           type: "default",
                           onClick: function (e) {
                              alert("Apakah anda yakin ingin mengedit data ini ? " + options.data.name)
                           }
                         }).appendTo(container);
                     }
                  },
                ]
//                onEditingStart: function(e) {
//                   console.log(e);
//                },
            }).dxDataGrid("instance");
});

